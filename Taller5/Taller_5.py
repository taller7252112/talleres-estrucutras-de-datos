from fastapi import FastAPI
import mock
app = FastAPI()

@app.get('/')
def root():
    return {
        "Servicio" : "Estructuras de datos"
    }

@app.post('/indices-invertidos')
def indices_invertidos(palabra: dict):
    cache = {}
    for indice, documento in enumerate(mock.my_documents):
        words = documento.lower().split()
        for word in words:
            if word in cache:
                cache[word].append((indice, documento))
            else:
                cache[word] = [(indice, documento)]

    return cache.get(palabra["palabra"], 'No se encontró')


"""def indices_invertidos(palabra: dict):
    cache = {}
    for indice, documento in enumerate(mock.my_documents):
        words = documento.lower().split()
        for word in words:
            if word in cache:
                cache[word].append(indice)
            else:
                cache[word] = [indice]

    return cache.get(palabra["palabra"], 'No se encontró')
"""


